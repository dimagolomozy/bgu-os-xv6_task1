#include "types.h"
#include "stat.h"
#include "user.h"

char buf;

void
read2(int fd)
{
	int n;

	while((n = read(fd, &buf, 1)) > 0)
	{
		if (buf == 'q')
		{
			// read the rest of the input
			while (read(fd, &buf, 1) > 0)
				if (buf == '\n') break;
			exit(1);
		}
		write(1, &buf, n);
	}
	if(n < 0)
	{
		printf(1, "cat: read error\n");
	    exit(0);
	}
}

int
main(int argc, char *argv[])
{
	read2(0);
	exit(1);
}
