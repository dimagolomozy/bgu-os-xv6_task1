#define NPROC			64  	// maximum number of processes
#define KSTACKSIZE 		4096  	// size of per-process kernel stack
#define NCPU          	8  		// maximum number of CPUs
#define NOFILE       	16 	 	// open files per process
#define NFILE       	100  	// open files per system
#define NINODE       	50  	// maximum number of active i-nodes
#define NDEV         	10  	// maximum major device number
#define ROOTDEV       	1  		// device number of file system root disk
#define MAXARG       	32  	// max exec arguments
#define MAXOPBLOCKS  	10  	// max # of blocks any FS op writes
#define LOGSIZE      	(MAXOPBLOCKS*3)  // max data sectors in on-disk log
#define NBUF         	(MAXOPBLOCKS*3)  // size of disk block cache
#define QUANTA		  	5	 	// The number of clock tick = 1 quanta
//Task 1.3
#define BLOCKING		1		// Blocking option
#define NONBLOCKING		0		// NonBlocking option

//Task 3:	SCHEDFLAG will be override from the makefile.
// we can delete this, but we leave it for safety
#ifndef SCHEDFLAG
	#define SCHEDFLAG	0		// DEFAULT=1 FRR=2 FCFS=3 CFS=4
#endif

